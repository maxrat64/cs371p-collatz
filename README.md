# CS371p: Object-Oriented Programming Collatz Repo

* Name: Max Rattray

* EID: mfr735

* GitLab ID: maxrat64

* HackerRank ID: maxrat964

* Git SHA: 4a1e43a9a99b5729806ae88a6dda88bc376997d4

* GitLab Pipelines: https://gitlab.com/maxrat64/cs371p-collatz/pipelines

* Estimated completion time: 8 hours

* Actual completion time: 9 hours

* Comments: I spent a lot of time on other approaches before I reached my
final solution.
