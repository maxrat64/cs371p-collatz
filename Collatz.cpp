// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// ------------
// collatz_eval
// ------------

int lazy_cache[1000000] = {0};

int calculate(int n) {
    int count = 1;
    long temp1 = (long)n;
    int cache_hit = 0;
    //calculate the cycle length for n, incrementing the number of steps at each
    //loop unless a number is already in the cache
    while(temp1 > 1) {
        //check if current number has a valid cache entry and use it if so
        if(temp1 < 1000000 && temp1 > 0 && lazy_cache[temp1] != 0) {
            cache_hit = lazy_cache[temp1];
            count += cache_hit - 1;
            break;
        }
        //if not, calculate the next number
        else {
            if(temp1 % 2 == 0) {
                temp1 >>= 1;
                count++;
            }
            else {
                temp1 = temp1 + (temp1 >> 1) + 1;
                count += 2;
            }
        }
    }
    long temp2 = (long)n;
    //distance is the number of numbers whose cycle lengths we can cache as a
    //result of caclulating n's cycle
    int distance = count - cache_hit;
    //start from n and go through the collatz steps, caching along the way,
    //until a value already in the cache or 1 is reached
    while(distance > 0) {
        if(temp2 < 1000000 && temp2 > 0 && lazy_cache[temp2] != 0)
            break;
        else {
            if(temp2 < 1000000 && temp2 > 0)
                lazy_cache[temp2] = cache_hit + distance;
            if(temp2 % 2 == 0)
                temp2 >>= 1;
            else
                temp2 = (temp2 * 3) + 1;
            distance--;
        }
    }
    return count;
}

int collatz_eval (int i, int j) {
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);
    int max = 0;
    //if the larger number is the first input, swap i and j
    if(j < i) {
        int temp = i;
        i = j;
        j = temp;
    }
    int n;
    /*only need to compute j/2 to j if i is less than half of j becuase there
    /is a number twice the size of i in this range and thus there is no reason
    /to compute values between i and j/2
    */
    if(j / 2 > i) {
        i = j / 2;
    }
    //loop through the range and either get the cycle length from the cache or
    //compute it and update the max cycle length if needed
    for(n = i; n <= j; n++) {
        int val;
        if(lazy_cache[n] != 0) {
            val = lazy_cache[n];
        }
        else {
            val = calculate(n);
        }
        if(val > max) {
            max = val;
        }
    }
    return max;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
